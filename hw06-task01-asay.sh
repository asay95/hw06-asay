#!/bin/bash
## HW6 ZIP Code Lookup (Page 182)
## Grabs information from city-data.com which has every ZIP code on its web page

## Create a variable for the url
URL="http://www.city-data.com/zips"
## Create a prompt for the user to enter their zip code of choosing
read -p "Enter a zip code: " zip
## Echo the message of the location that the ZIP code is in
echo -n "The ZIP code $zip is located in "
## Curl the website then start extracting information you only want and info
## you need to cut. The location we want is inside the paranthesis
## We can cut everything that's outside the paranthesis
curl -s "$URL/$zip.html" | grep -i "<title>" | cut -d\( -f2 | cut -d\) -f1

