#!/bin/bash
## HW6 Task 2 Area Code Lookup (Page 183)
## Use an area code to find what city the area code is from

URL="https://www.bennetyee.org/ucsd-pages/area.html"
read -p "Enter a three-digit US telephone area code: " areacode
## If the areacode entered is not 3 digits long, then prompt
## that only 3 digit numbered values can only be entered
if [ "$(echo $areacode | wc -c)" -ne 4 ]
then
        echo "Please enter in only three-digit US area codes."
elif [ ! -z "$(echo $areacode | sed 's/[[:digit:]]//g')" ]
then
	echo "Please enter in numbers only for the three-digit US area codes."
else
location="$(curl -s -dump $URL | grep "name=\"$areacode" | \
	sed 's/<[^>]*>//g;s/^ //g' | \
	cut -f2- -d\ | cut -f1 -d\( )"
echo "Area code $areacode =$location"
fi
